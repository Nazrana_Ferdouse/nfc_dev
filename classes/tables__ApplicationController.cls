/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ApplicationController {
    global ApplicationController() {

    }
    @RemoteAction
    global static String createGridViews(List<String> gridViewObjects) {
        return null;
    }
    @RemoteAction
    global static void deleteGridViews(List<String> recordIds) {

    }
    @RemoteAction
    global static String doMassUpdateRecords(tables.Models.GridRecordChangeRequest gridRecordChangeReq) {
        return null;
    }
    @RemoteAction
    global static String doMassUpdateRecordsMultiple(List<tables.Models.GridRecordChangeRequest> gridRecordChangeReqList) {
        return null;
    }
    @RemoteAction
    global static String editGridViews(List<String> gridViewObjects) {
        return null;
    }
    @RemoteAction
    global static String export() {
        return null;
    }
    @AuraEnabled
    global static String getApplicationInfo() {
        return null;
    }
    @RemoteAction
    global static String getChildRecords(tables.Models.ChildRecordRequest reletedRecordPayload) {
        return null;
    }
    global static Blob getContentVersionData() {
        return null;
    }
    @RemoteAction
    global static String getCustomLabels() {
        return null;
    }
    @RemoteAction
    global static String getGridView(tables.Models.GridViewRequest gridViewRequest) {
        return null;
    }
    @RemoteAction
    global static String getLookupRecords(String lookupParams) {
        return null;
    }
    @AuraEnabled
    global static String getNamespace() {
        return null;
    }
    @RemoteAction
    global static String getOrgMetadata() {
        return null;
    }
    @ReadOnly
    @RemoteAction
    global static tables.Models.OffsetResponse getRecordOffsetUsingView(tables.Models.OffsetRequest offsetRequest) {
        return null;
    }
    @ReadOnly
    @RemoteAction
    global static tables.Models.ViewRecordResponse getRecordsUsingViewLite(List<tables.Models.ViewRecordRequest> viewRecordReqs) {
        return null;
    }
    @RemoteAction
    global static String getRecordsUsingView(List<tables.Models.ViewRecordRequest> viewRecordReqs) {
        return null;
    }
    @RemoteAction
    global static String getRelatedListInfo(String relatedListInfoReq) {
        return null;
    }
    @RemoteAction
    global static String getRelatedViewsMap(List<String> viewDevNames) {
        return null;
    }
    @RemoteAction
    global static String getSObjectList() {
        return null;
    }
    @RemoteAction
    global static String getSObjectMetadataLite(List<String> objectApiNames) {
        return null;
    }
    @RemoteAction
    global static String getSObjectMetadata(List<String> objectApiNames) {
        return null;
    }
    @RemoteAction
    global static String getSObjectsMetadata(String metaDataReq) {
        return null;
    }
    @RemoteAction
    global static Map<String,Object> getSingleFileById(String fileId) {
        return null;
    }
    @RemoteAction
    global static String getTablesApplications(List<String> appParams) {
        return null;
    }
    @RemoteAction
    global static String getTablesObjectMap() {
        return null;
    }
    @RemoteAction
    global static String getUpdatedRecordChild(tables.Models.UpdateChildRequest updateRecordReq) {
        return null;
    }
    @RemoteAction
    global static String import(tables.Models.GridImportRequest gridImportRequest) {
        return null;
    }
    @AuraEnabled
    global static List<SObject> query(String queryTxt) {
        return null;
    }
    @RemoteAction
    global static String saveViewRules(List<String> gridViewObjects) {
        return null;
    }
    @Deprecated
    @RemoteAction
    global static String updateGridColumns(String objectApiName, String viewName, tables.Models.GridColumns gridColumns) {
        return null;
    }
    @Deprecated
    @RemoteAction
    global static String updateGridColumns(String objectApiName, String viewName, tables.Models.GridColumns gridColumns, String relatedChildInfoMap) {
        return null;
    }
    @RemoteAction
    global static String updateGridColumnsWRelatedViews(String objectApiName, String viewName, tables.Models.GridColumns gridColumns, String relatedChildInfoMap) {
        return null;
    }
    @RemoteAction
    global static String updateGridConfiguration(String objectApiName, String viewName, tables.Models.GridConfiguration gridConfiguration, tables.Models.GridUpdateConfiguration gridUpdateConfiguration) {
        return null;
    }
}
