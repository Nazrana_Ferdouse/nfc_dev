/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global virtual class FileModel {
    global Integer campaignId;
    global Integer folderId;
    @AuraEnabled
    global Integer id;
    @AuraEnabled
    global String name;
    global Integer trackerDomainId;
    global String url;
    global FileModel() {

    }
}
