/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Alignly_ConvertMatchedLeadsBatch implements Database.Batchable<SObject> {
    global algnlta__Alignly_Lead_to_Account_Settings__c listCodes;
    global Alignly_ConvertMatchedLeadsBatch(String currentContext) {

    }
    global void execute(Database.BatchableContext BC, List<Lead> scope) {

    }
    global void finish(Database.BatchableContext BC) {

    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return null;
    }
}
