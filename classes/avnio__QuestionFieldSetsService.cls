/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/ProjectQueFieldSets/*')
global class QuestionFieldSetsService {
    global QuestionFieldSetsService() {

    }
    @HttpPost
    global static String getQuestionFieldSet(String objectApiName, String fieldSetApiName) {
        return null;
    }
}
