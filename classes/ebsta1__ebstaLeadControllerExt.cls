/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ebstaLeadControllerExt {
    @RemoteAction
    global static ebsta1.SearchResultsPagedRequestV2 getSearchResultsPagedRequestV2Object(String leadId) {
        return null;
    }
    @RemoteAction
    global static String getTermSearch(String accountId) {
        return null;
    }
}
