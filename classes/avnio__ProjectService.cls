/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ProjectService {
    global ProjectService() {

    }
    global static avnio.ProjectService.SubmissionReviewResponse processApprovals(avnio.ProjectService.SubmitReviewRequest request) {
        return null;
    }
    global static avnio.ProjectService.SubmissionReviewResponse submitForApprovals(avnio.ProjectService.SubmitReviewRequest request) {
        return null;
    }
global class ReviewRequestItem {
    global ReviewRequestItem() {

    }
    global avnio.ProjectService.ReviewRequestItem setAction(String action) {
        return null;
    }
    global avnio.ProjectService.ReviewRequestItem setComments(String comments) {
        return null;
    }
    global avnio.ProjectService.ReviewRequestItem setRecordId(Id recordId) {
        return null;
    }
}
global class ReviewResponseItem {
    global ReviewResponseItem() {

    }
    global String getError() {
        return null;
    }
    global Boolean isSuccess() {
        return null;
    }
}
global class SubmissionReviewResponse {
    global SubmissionReviewResponse() {

    }
    global List<avnio.ProjectService.ReviewResponseItem> getResponseItems() {
        return null;
    }
}
global class SubmitReviewRequest {
    global SubmitReviewRequest() {

    }
    global avnio.ProjectService.SubmitReviewRequest add(avnio.ProjectService.ReviewRequestItem requestItem) {
        return null;
    }
}
}
