/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Models {
    global Models() {

    }
global class ChildRecordRequest {
    global ChildRecordRequest() {

    }
}
global class ChildRecordResult {
    global ChildRecordResult() {

    }
}
global class CustomOffset {
    global String createdDate;
    global String lastCreatedDate;
    global String lastName;
    global Id lastRecordId;
    global String name;
    global Id recordId;
    global CustomOffset() {

    }
    global tables.Models.CustomOffset withCreatedDate(Datetime createdDate) {
        return null;
    }
    global tables.Models.CustomOffset withLastCreatedDate(Datetime createdDate) {
        return null;
    }
    global tables.Models.CustomOffset withLastName(String name) {
        return null;
    }
    global tables.Models.CustomOffset withLastRecordId(String recordId) {
        return null;
    }
    global tables.Models.CustomOffset withName(String name) {
        return null;
    }
    global tables.Models.CustomOffset withRecordId(String recordId) {
        return null;
    }
}
global class FormattingRule {
    global Boolean active;
    global Boolean applyToRow;
    global String cellColor;
    global String columnLabel;
    global String columnName;
    global List<tables.Models.RuleCriterias> criterias;
    global String filter;
    global String fontColor;
    global Integer index;
    global Boolean isBold;
    global Boolean isItalic;
    global Boolean isUnderline;
    global String ruleName;
    global String ruleType;
    global FormattingRule() {

    }
}
global class GridColumn {
    global GridColumn() {

    }
}
global class GridColumns {
}
global class GridConfiguration {
}
global class GridExportRequest {
    global GridExportRequest() {

    }
}
global class GridExportResult {
    global List<tables__Object__c> grids;
    global List<tables__View__c> gridViews;
    global GridExportResult() {

    }
}
global class GridImportRequest {
    global tables.Models.GridExportResult gridDeploymentPackage;
    global Boolean overrideExistingView;
    global GridImportRequest() {

    }
}
global class GridImportResult {
    global Boolean isSuccess;
    global GridImportResult() {

    }
}
global class GridObjectConfiguration {
    global GridObjectConfiguration() {

    }
}
global class GridRecordChangeRequest {
    global GridRecordChangeRequest() {

    }
}
global class GridRecordChangeResult {
}
global class GridRecordUpdateRequest {
    global Map<String,List<tables.Models.RuleCriterias>> recordRuleMap;
    global GridRecordUpdateRequest() {

    }
}
global class GridUpdateConfiguration {
    global String chartsConfigJson;
    global Boolean updateChildFields;
    global Boolean updateFields;
    global Boolean updateFormattingRules;
    global GridUpdateConfiguration() {

    }
    global tables.Models.GridUpdateConfiguration get() {
        return null;
    }
}
global class GridView {
    global String baseViewId;
    global String chartsConfigJson;
    global tables.Models.GridConfiguration gridConfiguration;
    global tables.Models.GridObjectConfiguration gridObjectConfiguration;
    global Boolean isAdmin;
    global Boolean isDefault;
    global Boolean isOwner;
    global Boolean isSystemView;
    global String mainViewId;
    global String objectApiName;
    global String objectLabel;
    global Map<String,tables.Models.GridView> relatedViewsMap;
    global String source;
    global List<tables.ActionService.GridViewAction> viewActions;
    global String viewDescription;
    global String viewDevName;
    global String viewId;
    global String viewName;
    global String viewVisibility;
}
global class GridViewRequest {
    global Boolean includeRelatedViews;
    global List<String> objectNameSet;
    global List<String> viewNameSet;
    global GridViewRequest() {

    }
}
global class MassUpdateRequest {
    global List<tables.Models.RuleCriterias> criterias;
    global List<Id> recordIdSet;
    global MassUpdateRequest() {

    }
}
global class ObjectColumn {
    global List<tables.Models.GridColumn> fields;
    global String objectApiName;
    global String relationshipName;
    global ObjectColumn() {

    }
}
global class OffsetRequest {
    global Boolean includeInitialRecords;
    global Boolean isRestApi;
    global String lastRecordId;
    global String objectApiName;
    global String parentRecordId;
    global String relatedFieldApiName;
    global String relatedRecordId;
    global String viewDevName;
    global OffsetRequest() {

    }
}
global class OffsetResponse {
    global Boolean initialRecordsIncluded;
    global Map<String,tables.Models.CustomOffset> offsetMap;
    global List<tables.Models.CustomOffset> offsets;
    global tables.Models.ViewRecordResponse recordResponse;
    global OffsetResponse() {

    }
    global tables.Models.OffsetResponse addOffset(tables.Models.CustomOffset offset) {
        return null;
    }
    global tables.Models.OffsetResponse addOffset(Datetime createdDate, Id recordId, String name) {
        return null;
    }
}
global class RuleCriterias {
    global String fieldApexType;
    global String fieldLabel;
    global String fieldLookupObject;
    global String fieldReference;
    global String fieldType;
    global Integer index;
    global String objectApiName;
    global String operator;
    global String operatorType;
    global String referenceObjects;
    global String relationshipType;
    global String valueFieldApexType;
    global String valueFieldType;
    global String valueInput;
    global String valueType;
    global RuleCriterias() {

    }
}
global class UpdateChildRequest {
    global UpdateChildRequest() {

    }
}
global class ViewRecord {
    @AuraEnabled
    global Boolean hasMoreRecords;
    @AuraEnabled
    global String lastRecordId;
    @AuraEnabled
    global Integer recordChunkSize;
    @AuraEnabled
    global Integer recordCount;
    @AuraEnabled
    global List<SObject> records;
    @AuraEnabled
    global String viewDevName;
    global ViewRecord(String viewDevName) {

    }
    global tables.Models.ViewRecord get() {
        return null;
    }
    global tables.Models.ViewRecord hasMoreRecords() {
        return null;
    }
    global tables.Models.ViewRecord withRecordChunkSize(Integer recordChunkSize) {
        return null;
    }
    global tables.Models.ViewRecord withRecords(List<SObject> records) {
        return null;
    }
}
global class ViewRecordRequest {
}
global class ViewRecordResponse {
}
}
