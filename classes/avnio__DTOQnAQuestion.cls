/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class DTOQnAQuestion {
    global Boolean andOr {
        get;
        set;
    }
    global List<String> categories {
        get;
        set;
    }
    global String categoryFilter {
        get;
        set;
    }
    global String exactMatch {
        get;
        set;
    }
    global String Id {
        get;
        set;
    }
    global String QuestionText {
        get;
        set;
    }
    global String searchType {
        get;
        set;
    }
    global List<avnio.DTOQnAQuestion.MetaData> strictFilters {
        get;
        set;
    }
    global String tagFilter {
        get;
        set;
    }
    global List<String> tags {
        get;
        set;
    }
    global Integer Top {
        get;
        set;
    }
    global avnio.DTOUserActivity userActivity {
        get;
        set;
    }
    global DTOQnAQuestion() {

    }
    global DTOQnAQuestion(String id, String question) {

    }
    global DTOQnAQuestion(String id, String question, List<avnio.DTOQnAQuestion.MetaData> filters) {

    }
    global DTOQnAQuestion(String id, String question, Integer top) {

    }
global class MetaData {
    global String name {
        get;
        set;
    }
    global String value {
        get;
        set;
    }
    global MetaData() {

    }
    global MetaData(String name, String value) {

    }
}
}
