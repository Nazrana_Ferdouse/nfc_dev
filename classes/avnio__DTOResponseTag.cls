/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class DTOResponseTag {
    global String action {
        get;
        set;
    }
    global String id {
        get;
        set;
    }
    global String jid {
        get;
        set;
    }
    global String name {
        get;
        set;
    }
    global DTOResponseTag() {

    }
}
