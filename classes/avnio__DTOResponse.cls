/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class DTOResponse {
    global String answer {
        get;
        set;
    }
    global List<avnio.DTOResponseCategory> categories {
        get;
        set;
    }
    global String question {
        get;
        set;
    }
    global List<avnio.DTOResponseTag> tags {
        get;
        set;
    }
    global DTOResponse() {

    }
    global DTOResponse(avnio__Response__c response) {

    }
}
