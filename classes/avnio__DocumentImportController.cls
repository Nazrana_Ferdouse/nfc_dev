/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class DocumentImportController {
    global DocumentImportController() {

    }
    @RemoteAction
    global static String callApexMethod(String methodName, String methodParams) {
        return null;
    }
}
