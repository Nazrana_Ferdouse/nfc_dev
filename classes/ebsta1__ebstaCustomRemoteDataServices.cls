/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ebstaCustomRemoteDataServices {
    @RemoteAction
    global static String AdminTeamViewUpdate(Integer webUserTeamId, Integer securityId, Boolean valueField, List<ebsta1.AdminTeamViewRowTeam> teamRows) {
        return null;
    }
    @RemoteAction
    global static String GetAdditionalSettings() {
        return null;
    }
    @RemoteAction
    global static String GetAdminFileDownload() {
        return null;
    }
    @RemoteAction
    global static String GetAdminFileDownloadV2(String fileKeyId) {
        return null;
    }
    @RemoteAction
    global static String GetAdminReport() {
        return null;
    }
    @RemoteAction
    global static String GetAdminReportDownload() {
        return null;
    }
    @RemoteAction
    global static String GetAdminTeamView() {
        return null;
    }
    @RemoteAction
    global static String GetConnectedUsersWithMailboxes() {
        return null;
    }
    @RemoteAction
    global static String GetPreviewHeight() {
        return null;
    }
    @RemoteAction
    global static String GetReportingSettings() {
        return null;
    }
    @RemoteAction
    global static String LogEvent(String eventName, String actionTaken, Integer eventId) {
        return null;
    }
    @RemoteAction
    global static String LogPackageVersion() {
        return null;
    }
    @RemoteAction
    global static String SaveAdditionalSettings(String emailPreviewHeight, Boolean hideDefaultFilters) {
        return null;
    }
    @RemoteAction
    global static String SaveAdditionalSettingsV2(String emailPreviewHeight, Boolean hideDefaultFilters, Boolean hideMailboxConnectors) {
        return null;
    }
    @RemoteAction
    global static String SavePreviewHeight(String heightValue) {
        return null;
    }
    @RemoteAction
    global static String SetupAdminTeams() {
        return null;
    }
    @RemoteAction
    global static String UpdateReportingSettings(Boolean reportingOn, Boolean userReporting, Boolean updateCustomFields, Boolean upsertCustomObjects, Boolean accountReporting, Boolean contactReporting, Boolean leadReporting, Boolean opportunityReporting) {
        return null;
    }
    @RemoteAction
    global static String getLicenceStatusV2() {
        return null;
    }
}
