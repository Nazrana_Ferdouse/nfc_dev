/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/PublishLibrarySync/*')
global class LibraryPublishSyncApi {
    global LibraryPublishSyncApi() {

    }
    @HttpPost
    global static void syncPublishLibraryResults(avnio.LibraryPublishSyncApi.LibraryPublishSyncRequest request) {

    }
global class LibraryPublishSyncRequest {
    global String errorMsg;
    global Boolean isSuccess;
    global Integer op;
    global List<avnio.LibraryPublishSyncApi.LibraryPublishSyncResult> results;
    global LibraryPublishSyncRequest() {

    }
}
global class LibraryPublishSyncResult {
    global String externalId;
    global String recordId;
    global LibraryPublishSyncResult() {

    }
}
}
