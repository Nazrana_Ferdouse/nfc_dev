/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class DTORetrieveAnswerRequest {
    global Boolean isRetriveForAnsweredQuestions {
        get;
        set;
    }
    global List<String> projectQuestionIds {
        get;
        set;
    }
    global avnio.DTORetrieveAnswerRequest.RetrieveFilters retrievefilters {
        get;
        set;
    }
    global Decimal scoreThreshold {
        get;
        set;
    }
    global DTORetrieveAnswerRequest() {

    }
global class Category {
    global String filter {
        get;
        set;
    }
    global List<avnio.DTORetrieveAnswerRequest.Items> items {
        get;
        set;
    }
    global Category() {

    }
}
global class Items {
    global Integer i {
        get;
        set;
    }
    global String v {
        get;
        set;
    }
    global Items() {

    }
    global Items(Integer i, String v) {

    }
}
global class RetrieveFilters {
    global avnio.DTORetrieveAnswerRequest.Category category {
        get;
        set;
    }
    global String filter {
        get;
        set;
    }
    global RetrieveFilters() {

    }
}
}
