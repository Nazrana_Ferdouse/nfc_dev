/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global virtual class qsyd_Folder extends avnio.qsyd_Item {
    global qsyd_Folder() {

    }
    global qsyd_Folder(Id id, String folder, String label, String entityId) {

    }
    global qsyd_Folder(Id id, String folder, String label, String entityId, List<avnio.qsyd_Item> items) {

    }
    global avnio.qsyd_Folder convertToCanonical() {
        return null;
    }
    global avnio.qsyd_Folder load(avnio.ItemWrapperClass item) {
        return null;
    }
    global avnio.qsyd_Folder remove() {
        return null;
    }
    global avnio.qsyd_Folder save() {
        return null;
    }
}
