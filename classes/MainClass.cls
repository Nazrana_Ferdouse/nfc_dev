public with sharing class MainClass 
{
    // Instantiate HelperClass as variable "helper" of type HelperClass
    private HelperClass helper = new HelperClass();

    public Boolean randomMethod () 
    {
        // Reference the helperMethod() through newly created helper variable
        Boolean helperMethodValue = helper.helperMethod();
        return true;
    }
}