/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class RetrieveAnswersBatchController implements Database.AllowsCallouts, Database.Batchable<SObject>, Database.Stateful {
    global Boolean isRetriveForAnsweredQuestions;
    global String projectId;
    global Set<Id> projectQuestionIdsListForRetrival;
    global Integer recordsProcessed;
    global Decimal scoreThreshold;
    global RetrieveAnswersBatchController(String projectId, Decimal scoreThreshold) {

    }
    global RetrieveAnswersBatchController(String projectId, Decimal scoreThreshold, Set<Id> projectQuestionIdsListForRetrival) {

    }
    global void execute(Database.BatchableContext bc, List<avnio__ProjectQuestion__c> scope) {

    }
    global void finish(Database.BatchableContext bc) {

    }
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return null;
    }
}
