/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class qsyd_FolderList implements avnio.qsyd_IItemList {
    global qsyd_FolderList() {

    }
    global qsyd_FolderList(List<avnio.qsyd_Item> items) {

    }
    global avnio.qsyd_FolderList convertToCanonical() {
        return null;
    }
    global avnio.qsyd_FolderList convertToLogical() {
        return null;
    }
    global List<avnio__FileExplorerFolder__c> getCanonicalList() {
        return null;
    }
    global List<avnio.qsyd_Item> getLogicalList() {
        return null;
    }
    global avnio.qsyd_FolderList load(List<avnio.qsyd_Item> items) {
        return null;
    }
    global avnio.qsyd_FolderList retrieve(String recordId) {
        return null;
    }
}
