/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class DTOUpdateLibraryConfiguration {
    @InvocableVariable(label='Filters' description='Comma Seperated String; of Answer Methods will be used to filter the Projects Questions.' required=false)
    global String filters;
    @InvocableVariable(label='Add as Alternative Answer?' description='Answer will be added as Alternative Answer.' required=false)
    global Boolean isAlternativeAnswer;
    @InvocableVariable(label='Include Answer?' description='Answer will be included While Creating Response If this option is True.' required=false)
    global Boolean isIncludeAnswer;
    @InvocableVariable(label='Publish?' description='Library Sync Request will be published if this options is True.' required=false)
    global Boolean isPublish;
    @InvocableVariable(label='Record Id of Project' description='This Project questions will be created as Library Sync Requests.' required=true)
    global String projectId;
    @InvocableVariable(label='Project Question Ids' description='List of Project Question Ids to be processesed. If this is set, filters are ignored.' required=false)
    global List<String> projectQuestionIds;
    @InvocableVariable(label='Source for Library Sync Request' description='Source for the L:brary Sync Request' required=false)
    global String source;
    @InvocableVariable(label='Status for Library Sync Request' description='Status will be assigned to Library Sync Request.' required=false)
    global String status;
    @InvocableVariable(label='(Deprecated) Status Category for Response' description='Status category will be assigned to Response.' required=false)
    global String statusCategory;
    global DTOUpdateLibraryConfiguration() {

    }
}
