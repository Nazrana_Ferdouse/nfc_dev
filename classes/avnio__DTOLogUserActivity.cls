/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class DTOLogUserActivity {
    global String activity {
        get;
        set;
    }
    global String activityType {
        get;
        set;
    }
    global String content {
        get;
        set;
    }
    global String responseId {
        get;
        set;
    }
    global String searchId {
        get;
        set;
    }
    global DTOLogUserActivity() {

    }
}
