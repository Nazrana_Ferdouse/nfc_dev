/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class DTOUserActivity {
    global String channel {
        get;
        set;
    }
    global String Id {
        get;
        set;
    }
    global Boolean isLogUserActivity {
        get;
        set;
    }
    global String origin {
        get;
        set;
    }
    global String projectQuestionId {
        get;
        set;
    }
    global String transactionId {
        get;
        set;
    }
    global DTOUserActivity() {

    }
}
