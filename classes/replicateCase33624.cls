/*Purpose: This class contains static methods which are used by trgger TRG07
==============================================================================================================================================
The methods called perform following functionality:

•   Calculates Percent Allocation.
•   Inserts Sales Credit Line Item under Individual Sales Goal.
•   Sales Credit Line Item gets deleted on deletion of Sales Credit Record. 
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
1.0 -    Shashank   12/28/2012  Created this util class
2.0 -    Shashank   04/10/2013  Commented the Condition to check Opportunity Stage for req #647
3.0 -    Shashank   04/23/2013  Modified the validation criteria to exclude Employee LOB = 'Region Market Development' for req #752
4.0 -     Gyan      25/10/2013  Modified to opportunity Team Member record for Respective Sales-credit Record if it not Exist already.
5.0 -     Gyan      21/11/2013  Modified As per Req-3477 As New Collegue lookup has been introduced in Individual Sales goal record. 
6.0 -     Venkat    2/7/2016    Updated code as part of Request 9599(Feb Release 2016)  
============================================================================================================================================== 
*/

public class replicateCase33624 {
/*Purpose: This class contains static methods which are used by trgger TRG07
==============================================================================================================================================
The methods called perform following functionality:

•   Calculates Percent Allocation.
•   Inserts Sales Credit Line Item under Individual Sales Goal.
•   Sales Credit Line Item gets deleted on deletion of Sales Credit Record. 
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
1.0 -    Shashank   12/28/2012  Created this util class
2.0 -    Shashank   04/10/2013  Commented the Condition to check Opportunity Stage for req #647
3.0 -    Shashank   04/23/2013  Modified the validation criteria to exclude Employee LOB = 'Region Market Development' for req #752
4.0 -     Gyan      25/10/2013  Modified to opportunity Team Member record for Respective Sales-credit Record if it not Exist already.
5.0 -     Gyan      21/11/2013  Modified As per Req-3477 As New Collegue lookup has been introduced in Individual Sales goal record. 
6.0 -     Venkat    2/7/2016    Updated code as part of Request 9599(Feb Release 2016)  
============================================================================================================================================== 
*/
string Role, LOB, Country,Region;
/*Purpose: This class contains static methods which are used by trgger TRG07
==============================================================================================================================================
The methods called perform following functionality:

•   Calculates Percent Allocation.
•   Inserts Sales Credit Line Item under Individual Sales Goal.
•   Sales Credit Line Item gets deleted on deletion of Sales Credit Record. 
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
1.0 -    Shashank   12/28/2012  Created this util class
2.0 -    Shashank   04/10/2013  Commented the Condition to check Opportunity Stage for req #647
3.0 -    Shashank   04/23/2013  Modified the validation criteria to exclude Employee LOB = 'Region Market Development' for req #752
4.0 -     Gyan      25/10/2013  Modified to opportunity Team Member record for Respective Sales-credit Record if it not Exist already.
5.0 -     Gyan      21/11/2013  Modified As per Req-3477 As New Collegue lookup has been introduced in Individual Sales goal record. 
6.0 -     Venkat    2/7/2016    Updated code as part of Request 9599(Feb Release 2016)  
============================================================================================================================================== 
*/
/*Purpose: This class contains static methods which are used by trgger TRG07
==============================================================================================================================================
The methods called perform following functionality:

•   Calculates Percent Allocation.
•   Inserts Sales Credit Line Item under Individual Sales Goal.
•   Sales Credit Line Item gets deleted on deletion of Sales Credit Record. 
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
1.0 -    Shashank   12/28/2012  Created this util class
2.0 -    Shashank   04/10/2013  Commented the Condition to check Opportunity Stage for req #647
3.0 -    Shashank   04/23/2013  Modified the validation criteria to exclude Employee LOB = 'Region Market Development' for req #752
4.0 -     Gyan      25/10/2013  Modified to opportunity Team Member record for Respective Sales-credit Record if it not Exist already.
5.0 -     Gyan      21/11/2013  Modified As per Req-3477 As New Collegue lookup has been introduced in Individual Sales goal record. 
6.0 -     Venkat    2/7/2016    Updated code as part of Request 9599(Feb Release 2016)  
============================================================================================================================================== 
*/
}