/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PipelineHistoryController extends IS2_TGH.ISPHController {
    @RemoteAction
    global static Boolean resetStageList() {
        return null;
    }
    @RemoteAction
    global static Boolean setInterval(String interval) {
        return null;
    }
    @RemoteAction
    global static Boolean setStageList(String jsonStr) {
        return null;
    }
}
