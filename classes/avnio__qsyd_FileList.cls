/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class qsyd_FileList implements avnio.qsyd_IItemList {
    global qsyd_FileList() {

    }
    global qsyd_FileList(List<avnio.qsyd_Item> items) {

    }
    global avnio.qsyd_FileList convertToCanonical() {
        return null;
    }
    global avnio.qsyd_FileList convertToLogical() {
        return null;
    }
    global List<avnio__FileExplorerFile__c> getCanonicalList() {
        return null;
    }
    global List<avnio.qsyd_Item> getLogicalList() {
        return null;
    }
    global avnio.qsyd_FileList load(List<avnio.qsyd_Item> items) {
        return null;
    }
    global avnio.qsyd_FileList retrieve(String recordId) {
        return null;
    }
}
