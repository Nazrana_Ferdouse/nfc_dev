/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ResponseMigrationBatchService implements Database.Batchable<SObject> {
    global Set<Id> responseIds;
    global Boolean runLastUsedDateUpdateBatch;
    global ResponseMigrationBatchService() {

    }
    global void execute(Database.BatchableContext bc, List<SObject> scope) {

    }
    global void finish(Database.BatchableContext bc) {

    }
    global static Id invokeLastUsedUpdateBatch() {
        return null;
    }
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return null;
    }
}
