/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global virtual class qsyd_File extends avnio.qsyd_Item {
    global qsyd_File() {

    }
    global qsyd_File(Id id, Id folder, String label, String documentId, String entityId, String type, String ext, String owner, String tags, Decimal size, List<avnio.qsyd_Item> items) {

    }
    global avnio.qsyd_File convertToCanonical() {
        return null;
    }
    global avnio.qsyd_File load(avnio.ItemWrapperClass item) {
        return null;
    }
    global avnio.qsyd_File remove() {
        return null;
    }
    global avnio.qsyd_File save() {
        return null;
    }
}
