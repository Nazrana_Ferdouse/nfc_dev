/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/Response/*')
global class ResponseService {
    global ResponseService() {

    }
    @HttpPost
    global static String createResponse(avnio.DTOResponse response) {
        return null;
    }
    @HttpDelete
    global static Boolean deleteResponse() {
        return null;
    }
    @HttpPut
    global static Boolean publishResponse(List<String> responseIds) {
        return null;
    }
    @HttpPatch
    global static String updateResponse(avnio.DTOResponse response) {
        return null;
    }
}
