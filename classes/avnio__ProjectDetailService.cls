/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/ProjectDetails/*')
global class ProjectDetailService {
    global ProjectDetailService() {

    }
    @HttpPost
    global static String getProjectDetails(String projectId, String objectApiName, String fieldSetApiName) {
        return null;
    }
}
