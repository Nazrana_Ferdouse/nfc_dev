/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class RichTextVFCmpController {
    @RemoteAction
    global static String addWordToUserDictionary(String word) {
        return null;
    }
    @RemoteAction
    global static String autoCompleteAPI(String autoCompleteRequest) {
        return null;
    }
    @RemoteAction
    global static String removeWordFromUserDictionary(String word) {
        return null;
    }
}
