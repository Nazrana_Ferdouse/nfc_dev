/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Alignly_BulkMatchLeadsBatch implements Database.Batchable<SObject> {
    global algnlta__Alignly_Lead_to_Account_Settings__c listCodes;
    global void execute(Database.BatchableContext bc, List<Lead> leadList) {

    }
    global void finish(Database.BatchableContext bc) {

    }
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return null;
    }
}
