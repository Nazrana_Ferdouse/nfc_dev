/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global virtual class DynamicContentVariationModel {
    global String content;
    global Integer dynamicContentId;
    global Integer id;
    global String operator;
    global String value1;
    global String value2;
    global DynamicContentVariationModel() {

    }
}
