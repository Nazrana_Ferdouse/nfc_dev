<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <customSettingsType>Hierarchy</customSettingsType>
    <description>Manage settings for Align.ly Lead-to-Account Matching.</description>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>algnlta__Account_Field_for_City_Match__c</fullName>
        <defaultValue>&quot;BillingCity&quot;</defaultValue>
        <deprecated>false</deprecated>
        <description>API Name of City field on Account for which City field on Lead will be matched. For use in Geography Match.</description>
        <externalId>false</externalId>
        <inlineHelpText>API Name of City field on Account for which City field on Lead will be matched. For use in Geography Match.</inlineHelpText>
        <label>Account Field for City Match</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>algnlta__Account_Field_for_Country_Match__c</fullName>
        <defaultValue>&quot;BillingCountry&quot;</defaultValue>
        <deprecated>false</deprecated>
        <description>API Name of Country field on Account for which Country field on Lead will be matched. For use in Geography Match.</description>
        <externalId>false</externalId>
        <inlineHelpText>API Name of Country field on Account for which Country field on Lead will be matched. For use in Geography Match.</inlineHelpText>
        <label>Account Field for Country Match</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>algnlta__Account_Field_for_Postal_Code_Match__c</fullName>
        <defaultValue>&quot;BillingPostalCode&quot;</defaultValue>
        <deprecated>false</deprecated>
        <description>API Name of Postal Code field on Account for which Postal Code field on Lead will be matched. For use in Geography Match.</description>
        <externalId>false</externalId>
        <inlineHelpText>API Name of Postal Code field on Account for which Postal Code field on Lead will be matched. For use in Geography Match.</inlineHelpText>
        <label>Account Field for Postal Code Match</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>algnlta__Account_Field_for_State_Match__c</fullName>
        <defaultValue>&quot;BillingState&quot;</defaultValue>
        <deprecated>false</deprecated>
        <description>API Name of State field on Account for which State field on Lead will be matched. For use in Geography Match.</description>
        <externalId>false</externalId>
        <inlineHelpText>API Name of State field on Account for which State field on Lead will be matched. For use in Geography Match.</inlineHelpText>
        <label>Account Field for State Match</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>algnlta__Account_Owner_API_Name__c</fullName>
        <defaultValue>&quot;OwnerId&quot;</defaultValue>
        <deprecated>false</deprecated>
        <description>API Name of Owner ID field on Account for use with &quot;Set Lead Owner to Account Owner&quot;, &quot;Set Contact Owner to Account Owner&quot;, and &quot;Realign&quot; features.</description>
        <externalId>false</externalId>
        <inlineHelpText>API Name of Owner ID field on Account for use with &quot;Set Lead Owner to Account Owner&quot;, &quot;Set Contact Owner to Account Owner&quot;, and &quot;Realign&quot; features.</inlineHelpText>
        <label>Account Owner API Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>algnlta__Batch_Scope_Size_Contact_Realign__c</fullName>
        <defaultValue>200</defaultValue>
        <deprecated>false</deprecated>
        <description>The number of Contact records processed in a batch when realigning Contact Owners. Default is 200 records.</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of Contact records processed in a batch when realigning Contact Owners. Default is 200 records.</inlineHelpText>
        <label>Batch Scope Size Contact Realign</label>
        <precision>4</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>algnlta__Batch_Scope_Size_Convert_Matched_Leads__c</fullName>
        <defaultValue>200</defaultValue>
        <deprecated>false</deprecated>
        <description>The number of records processed in a batch when converting matched Leads to Contacts. Default is 200 records.</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of records processed in a batch when converting matched Leads to Contacts. Default is 200 records.</inlineHelpText>
        <label>Batch Scope Size Convert Matched Leads</label>
        <precision>4</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>algnlta__Batch_Scope_Size_Match_Leads_to_Accounts__c</fullName>
        <defaultValue>200</defaultValue>
        <deprecated>false</deprecated>
        <description>The number of records processed in a batch when matching Leads to Accounts. Default is 200 records.</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of records processed in a batch when matching Leads to Accounts. Default is 200 records.</inlineHelpText>
        <label>Batch Scope Size Match Leads to Accounts</label>
        <precision>4</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>algnlta__Batch_Scope_Size_Matched_Lead_Realign__c</fullName>
        <defaultValue>200</defaultValue>
        <deprecated>false</deprecated>
        <description>The number of Lead records processed in a batch when realigning Lead Owners of Leads with a Matched Account. Default is 200 records.</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of Lead records processed in a batch when realigning Lead Owners of Leads with a Matched Account. Default is 200 records.</inlineHelpText>
        <label>Batch Scope Size Matched Lead Realign</label>
        <precision>4</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>algnlta__Batch_Scope_Size_Unmatched_Lead_Realign__c</fullName>
        <defaultValue>200</defaultValue>
        <deprecated>false</deprecated>
        <description>The number of Lead records processed in a batch when realigning Lead Owners of Leads without a Matched Account. Default is 200 records.</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of Lead records processed in a batch when realigning Lead Owners of Leads without a Matched Account. Default is 200 records.</inlineHelpText>
        <label>Batch Scope Size Unmatched Lead Realign</label>
        <precision>4</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>algnlta__Batch_Scope_Size_Update_Email_Domains__c</fullName>
        <defaultValue>200</defaultValue>
        <deprecated>false</deprecated>
        <description>The number of records processed in a batch when updating Email Domain(s) on Account. Default is 200 records.</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of records processed in a batch when updating Email Domain(s) on Account. Default is 200 records.</inlineHelpText>
        <label>Batch Scope Size Update Email Domains</label>
        <precision>4</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>algnlta__Converted_Status__c</fullName>
        <defaultValue>&quot;Auto-Converted&quot;</defaultValue>
        <deprecated>false</deprecated>
        <description>API Name of the Lead Status picklist value to be used when a Lead is auto-convert to a Contact attached to their matched Account.</description>
        <externalId>false</externalId>
        <inlineHelpText>API Name of the Lead Status picklist value to be used when a Lead is auto-convert to a Contact attached to their matched Account.</inlineHelpText>
        <label>Converted Status</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>algnlta__Lead_Field_for_City_Match__c</fullName>
        <defaultValue>&quot;City&quot;</defaultValue>
        <deprecated>false</deprecated>
        <description>API Name of City field on Lead to be matched to Account for use in Geography Match.</description>
        <externalId>false</externalId>
        <inlineHelpText>API Name of City field on Lead to be matched to Account for use in Geography Match.</inlineHelpText>
        <label>Lead Field for City Match</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>algnlta__Lead_Field_for_Country_Match__c</fullName>
        <defaultValue>&quot;Country&quot;</defaultValue>
        <deprecated>false</deprecated>
        <description>API Name of Country field on Lead to be matched to Account for use in Geography Match.</description>
        <externalId>false</externalId>
        <inlineHelpText>API Name of Country field on Lead to be matched to Account for use in Geography Match.</inlineHelpText>
        <label>Lead Field for Country Match</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>algnlta__Lead_Field_for_Postal_Code_Match__c</fullName>
        <defaultValue>&quot;PostalCode&quot;</defaultValue>
        <deprecated>false</deprecated>
        <description>API Name of Postal Code field on Lead to be matched to Account for use in Geography Match.</description>
        <externalId>false</externalId>
        <inlineHelpText>API Name of Postal Code field on Lead to be matched to Account for use in Geography Match.</inlineHelpText>
        <label>Lead Field for Postal Code Match</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>algnlta__Lead_Field_for_State_Match__c</fullName>
        <defaultValue>&quot;State&quot;</defaultValue>
        <deprecated>false</deprecated>
        <description>API Name of State field on Lead to be matched to Account for use in Geography Match.</description>
        <externalId>false</externalId>
        <inlineHelpText>API Name of State field on Lead to be matched to Account for use in Geography Match.</inlineHelpText>
        <label>Lead Field for State Match</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>algnlta__On_Demand_Contact_Owner_Realign__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If TRUE, when the &quot;Run All Align.ly Jobs in Order&quot; button is clicked or the &quot;Run Matched Lead Realign&quot; button is clicked, eligible Contacts will have their Contact Owner realigned.</description>
        <externalId>false</externalId>
        <inlineHelpText>If TRUE, when the &quot;Run All Align.ly Jobs in Order&quot; button is clicked or the &quot;Run Matched Lead Realign&quot; button is clicked, eligible Contacts will have their Contact Owner realigned.</inlineHelpText>
        <label>On-Demand Contact Owner Realign</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>algnlta__On_Demand_Lead_Auto_Conversion__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If TRUE, when the &quot;Run All Align.ly Jobs in Order&quot; button is clicked or the &quot;3. Convert Leads to Matched Accounts&quot; button is clicked, all Leads with a Matched Account will convert to a Contact attached to their Matched Account.</description>
        <externalId>false</externalId>
        <inlineHelpText>If TRUE, when the &quot;Run All Align.ly Jobs in Order&quot; button is clicked or the &quot;3. Convert Leads to Matched Accounts&quot; button is clicked, all Leads with a Matched Account will convert to a Contact attached to their Matched Account.</inlineHelpText>
        <label>On-Demand Lead Auto-Conversion</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>algnlta__On_Demand_Lead_to_Account_Matching__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If TRUE, when the &quot;Run All Align.ly Jobs in Order&quot; button is clicked or the &quot;2. Matched Leads to Accounts&quot; button is clicked, all unmatched Leads will attempt to be matched to an Account.</description>
        <externalId>false</externalId>
        <inlineHelpText>If TRUE, when the &quot;Run All Align.ly Jobs in Order&quot; button is clicked or the &quot;2. Matched Leads to Accounts&quot; button is clicked, all unmatched Leads will attempt to be matched to an Account.</inlineHelpText>
        <label>On-Demand Lead-to-Account Matching</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>algnlta__On_Demand_Matched_Lead_Owner_Realign__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If TRUE, when the &quot;Run All Align.ly Jobs in Order&quot; button is clicked or the &quot;Run Matched Lead Realign&quot; button is clicked, eligible Leads with a Matched Account will have their Lead Owner realigned.</description>
        <externalId>false</externalId>
        <inlineHelpText>If TRUE, when the &quot;Run All Align.ly Jobs in Order&quot; button is clicked or the &quot;Run Matched Lead Realign&quot; button is clicked, eligible Leads with a Matched Account will have their Lead Owner realigned.</inlineHelpText>
        <label>On-Demand Matched Lead Owner Realign</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>algnlta__On_Demand_Unmatched_Lead_Owner_Realign__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If TRUE, when the &quot;Run All Align.ly Jobs in Order&quot; button is clicked or the &quot;Run Unmatched Lead Realign&quot; button is clicked, eligible Leads without a Matched Account will be re-run through Lead Assignment Rules.</description>
        <externalId>false</externalId>
        <inlineHelpText>If TRUE, when the &quot;Run All Align.ly Jobs in Order&quot; button is clicked or the &quot;Run Unmatched Lead Realign&quot; button is clicked, eligible Leads without a Matched Account will be re-run through Lead Assignment Rules.</inlineHelpText>
        <label>On-Demand Unmatched Lead Owner Realign</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>algnlta__On_Demand_Update_Email_Domains__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If TRUE, when the &quot;Run All Align.ly Jobs in Order&quot; button is clicked or the &quot;1. Update Email Domains on Account&quot; button is clicked, all Contacts and Accounts will be processed to update the &quot;Email Domain(s)&quot; field on Account.</description>
        <externalId>false</externalId>
        <inlineHelpText>If TRUE, when the &quot;Run All Align.ly Jobs in Order&quot; button is clicked or the &quot;1. Update Email Domains on Account&quot; button is clicked, all Contacts and Accounts will be processed to update the &quot;Email Domain(s)&quot; field on Account.</inlineHelpText>
        <label>On-Demand Update Email Domains</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>algnlta__Real_Time_Lead_to_Account_Matching__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If TRUE, when a Lead is created, the Lead will be matched to an Account in real-time.</description>
        <externalId>false</externalId>
        <inlineHelpText>If TRUE, when a Lead is created, the Lead will be matched to an Account in real-time.</inlineHelpText>
        <label>Real-Time Lead-to-Account Matching</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>algnlta__Scheduled_Contact_Owner_Realign__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If TRUE, when Align.ly Lead-to-Account is scheduled to run, eligible Contacts will have their Contact Owner realigned.</description>
        <externalId>false</externalId>
        <inlineHelpText>If TRUE, when Align.ly Lead-to-Account is scheduled to run, eligible Contacts will have their Contact Owner realigned.</inlineHelpText>
        <label>Scheduled Contact Owner Realign</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>algnlta__Scheduled_Lead_Auto_Conversion__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If TRUE, when Align.ly Lead-to-Account is scheduled to run, all Leads with a Matched Account will convert to a Contact attached to their Matched Account.</description>
        <externalId>false</externalId>
        <inlineHelpText>If TRUE, when Align.ly Lead-to-Account is scheduled to run, all Leads with a Matched Account will convert to a Contact attached to their Matched Account.</inlineHelpText>
        <label>Scheduled Lead Auto-Conversion</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>algnlta__Scheduled_Lead_to_Account_Matching__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If TRUE, when Align.ly Lead-to-Account is scheduled to run, all unmatched Leads will attempt to be matched to an Account.</description>
        <externalId>false</externalId>
        <inlineHelpText>If TRUE, when Align.ly Lead-to-Account is scheduled to run, all unmatched Leads will attempt to be matched to an Account.</inlineHelpText>
        <label>Scheduled Lead-to-Account Matching</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>algnlta__Scheduled_Matched_Lead_Owner_Realign__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If TRUE, when Align.ly Lead-to-Account is scheduled to run, eligible Leads with a Matched Account will have their Lead Owner realigned.</description>
        <externalId>false</externalId>
        <inlineHelpText>If TRUE, when Align.ly Lead-to-Account is scheduled to run, eligible Leads with a Matched Account will have their Lead Owner realigned.</inlineHelpText>
        <label>Scheduled Matched Lead Owner Realign</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>algnlta__Scheduled_Unmatched_Lead_Owner_Realign__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If TRUE, when Align.ly Lead-to-Account is scheduled to run, eligible Leads without a Matched Account will be re-run through Lead Assignment Rules.</description>
        <externalId>false</externalId>
        <inlineHelpText>If TRUE, when Align.ly Lead-to-Account is scheduled to run, eligible Leads without a Matched Account will be re-run through Lead Assignment Rules.</inlineHelpText>
        <label>Scheduled Unmatched Lead Owner Realign</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>algnlta__Scheduled_Update_Email_Domains__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If TRUE, when Align.ly Lead-to-Account is scheduled to run, all Contacts and Accounts will be processed to update the &quot;Email Domain(s)&quot; field on Account.</description>
        <externalId>false</externalId>
        <inlineHelpText>If TRUE, when Align.ly Lead-to-Account is scheduled to run, all Contacts and Accounts will be processed to update the &quot;Email Domain(s)&quot; field on Account.</inlineHelpText>
        <label>Scheduled Update Email Domains</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>algnlta__Set_Contact_Owner_to_Account_Owner__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If TRUE, Contact Owner will be updated to the &quot;Account Owner API Name&quot; when a Lead is auto-converted to an Account.</description>
        <externalId>false</externalId>
        <inlineHelpText>If TRUE, Contact Owner will be updated to the &quot;Account Owner API Name&quot; when a Lead is auto-converted to an Account.</inlineHelpText>
        <label>Set Contact Owner to Account Owner</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>algnlta__Set_Lead_Owner_to_Account_Owner__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If TRUE, Lead Owner will be updated to the &quot;Account Owner API Name&quot; when a Lead is matched to an Account.</description>
        <externalId>false</externalId>
        <inlineHelpText>If TRUE, Lead Owner will be updated to the &quot;Account Owner API Name&quot; when a Lead is matched to an Account.</inlineHelpText>
        <label>Set Lead Owner to Account Owner</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>algnlta__WHERE_Clause_for_Contact_Realign__c</fullName>
        <deprecated>false</deprecated>
        <description>SOQL WHERE clause to only update Contact Owners for a subset of Contacts (example: Realign__c = True). If blank, all eligible Contact records will be updated.</description>
        <externalId>false</externalId>
        <inlineHelpText>SOQL WHERE clause to only update Contact Owners for a subset of Contacts (example: Realign__c = True). If blank, all eligible Contact records will be updated.</inlineHelpText>
        <label>WHERE Clause for Contact Realign</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>algnlta__WHERE_Clause_for_Matched_Lead_Realign__c</fullName>
        <deprecated>false</deprecated>
        <description>SOQL WHERE clause to only update Leads Owners for a subset of Leads with a Matched Account (example: Realign__c = True). If blank, all eligible Leads records will be updated.</description>
        <externalId>false</externalId>
        <inlineHelpText>SOQL WHERE clause to only update Leads Owners for a subset of Leads with a Matched Account (example: Realign__c = True). If blank, all eligible Leads records will be updated.</inlineHelpText>
        <label>WHERE Clause for Matched Lead Realign</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>algnlta__WHERE_Clause_for_Unmatched_Lead_Realign__c</fullName>
        <deprecated>false</deprecated>
        <description>SOQL WHERE clause to only update Leads Owners for a subset of Leads without a Matched Account (example: Realign__c = True). If blank, all eligible Leads records will be updated.</description>
        <externalId>false</externalId>
        <inlineHelpText>SOQL WHERE clause to only update Leads Owners for a subset of Leads without a Matched Account (example: Realign__c = True). If blank, all eligible Leads records will be updated.</inlineHelpText>
        <label>WHERE Clause for Unmatched Lead Realign</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Align.ly Lead-to-Account Settings</label>
    <visibility>Public</visibility>
</CustomObject>