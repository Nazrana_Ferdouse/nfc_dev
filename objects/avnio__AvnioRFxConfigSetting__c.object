<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <customSettingsType>Hierarchy</customSettingsType>
    <description>This custom setting contains configuration fields that are used in various places in the Avnio.</description>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>avnio__DeleteArchivedResponses__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Delete Archived Responses</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>avnio__DisableAlternativeAnswerTrigger__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Disable Alternative Answer Trigger</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>avnio__DisableAlternativeQuestionTrigger__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Disable Alternative Question Trigger</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>avnio__DisableLibrarySearchActivityLogging__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If enabled, it will not log any user activity related to Library Search</description>
        <externalId>false</externalId>
        <inlineHelpText>If enabled, it will not log any user activity related to Library Search</inlineHelpText>
        <label>Disable Library Search Activity Logging</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>avnio__DisableProjectQuestionTrigger__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Check this to disable avnio__ProjectQuestion__c trigger.</description>
        <externalId>false</externalId>
        <inlineHelpText>Check this to disable avnio__ProjectQuestion__c trigger.</inlineHelpText>
        <label>Disable Project Question Trigger</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>avnio__DisableProjectTrigger__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Check this to disable avnio__Project__c trigger.</description>
        <externalId>false</externalId>
        <inlineHelpText>Check this to disable avnio__Project__c trigger.</inlineHelpText>
        <label>Disable Project Trigger</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>avnio__DisableResponseCountUpdate__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Check this to disable Response Number of Times Used, Last Used calculations.</description>
        <externalId>false</externalId>
        <inlineHelpText>Check this to disable Response Number of Times Used, Last Used calculations.</inlineHelpText>
        <label>Disable Response Count Update</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>avnio__DisableResponseTrigger__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Check this to disable avnio__Response__c trigger.</description>
        <externalId>false</externalId>
        <inlineHelpText>Check this to disable avnio__Response__c trigger.</inlineHelpText>
        <label>Disable Response Trigger</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>avnio__DisableResponseVersionTrigger__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Check this to disable avnio__ResponseVersion__c trigger.</description>
        <externalId>false</externalId>
        <inlineHelpText>Check this to disable avnio__ResponseVersion__c trigger.</inlineHelpText>
        <label>Disable Response Version Trigger</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>avnio__DisableUserDictionaryTrigger__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Disable UserDictionary Trigger</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>avnio__EnableSubCategorySelection__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Enable Sub Category for Word while Project Configuration</description>
        <externalId>false</externalId>
        <inlineHelpText>Select this enable Sub Category selection for a word document only during Project Configuration. Sub Category is not available for Library Import.</inlineHelpText>
        <label>Enable Sub Category Selection</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>avnio__InternalReviewTemplateId__c</fullName>
        <deprecated>false</deprecated>
        <description>Content Version Id of the template that is going to be used to generate RFP for Internal Review .</description>
        <externalId>false</externalId>
        <inlineHelpText>Content Version Id of the template that is going to be used to generate RFP for Internal Review .</inlineHelpText>
        <label>Internal Review Template Id</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>avnio__IsDisableProjectQuestionValidation__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Disable Project Question Validation</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>avnio__KeywordSearchResultToShow__c</fullName>
        <deprecated>false</deprecated>
        <description>When a users searches the Library using keyword search, this will determine how many top results to show to the users.</description>
        <externalId>false</externalId>
        <inlineHelpText>When a users searches the Library using keyword search, this will determine how many top results to show to the users.</inlineHelpText>
        <label>Keyword Search Result To Show</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>avnio__LibrarySearchResultToShow__c</fullName>
        <deprecated>false</deprecated>
        <description>When a users searches the Library, this will determine how many top results to show to the users.</description>
        <externalId>false</externalId>
        <inlineHelpText>When a users searches the Library, this will determine how many top results to show to the users.</inlineHelpText>
        <label>Library Search Result To Show</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>avnio__OverrideLibraryFilter__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Override Library Filter (Sandbox Only)</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Avnio RFx Config Setting</label>
    <visibility>Public</visibility>
</CustomObject>